#!/usr/bin/env python3

import os
import wave
import glob
import argparse
from subprocess import call

class Image(object):
    def __init__(self, text):
        self._text = text
    def generate(self, filename):
        call(["convert",
              "-size", SIZE,
              "-background", "black",
              "-pointsize", "25",
              "-fill", "white",
              "-interline-spacing", "6",
              "-font", FONT,
              "-gravity", "NorthWest",
              "caption:%s"%self._text,
              "-bordercolor", "black",
              "-border", "10",
              filename])

class VideoImages(object):
    def __init__(self, filename):
        self._filename = filename
        self._framen = 0
        
    def generate(self, text, count):
        if count > 0:
            image = Image(text)
            image.generate(self._filename %(self._framen))
        for i in range(self._framen+1, self._framen+count):
            os.link(self._filename%(self._framen),
                    self._filename%(i))
        self._framen = self._framen + count

class Speech(object):
    def __init__(self, text):
        self._text = text
    def generate(self, filename):
        call(["pico2wave",
              "--wave", filename,
              "--lang", LANG, self._text])

class VideoSpeech(object):
    def __init__(self, filename):
        self._filename = filename
        self._wave = None
        self._framerate = 0

    def generate(self, text):
        sp = Speech(text)
        filename = os.path.join(TMPDIR, "temp.wav")
        sp.generate(filename)

        w = wave.open(filename, 'r')
        nframes = w.getnframes()

        if not self._wave:
            self._wave = wave.open(self._filename, 'w')
            self._wave.setparams(w.getparams())
            self._framerate = w.getframerate()

        samples = True
        while samples:
            samples = w.readframes(1000)
            self._wave.writeframes(samples)
        w.close()
        s = 1.0 * nframes / self._framerate
        print ("%d frames, %f seconds"%(nframes, s))
        return s

    def close(self):
        self._wave.close()

class Splitter(object):
    MAX_WORDS_PER_PAGE = 50
    MAX_CHARS_PER_PAGE = 500
    ENDOFWORD = " \n"

    def __init__(self, text):
        self._text = text

    def split(self):
        pages = []
        page = ""
        nwords = 0
        nchars = 0
        for c in self._text:
            if self.ENDOFWORD.find(c) != -1:
                nwords = nwords + 1
            nchars = nchars + 1
            page = page + c
            if nwords == self.MAX_WORDS_PER_PAGE or nchars == self.MAX_CHARS_PER_PAGE:
                pages.append(page)
                nchars = 0
                nwords = 0
                page = ""
        if page:
            pages.append(page)

        return pages

class Splitter2(object):
    SOFT_MAX_WORDS = 50
    HARD_MAX_WORDS = 70
    SOFT_MAX_CHARS = 500
    HARD_MAX_CHARS = 500
    ENDOFWORD = " \n"
    ENDOFPAGE = ",.\n"
    def __init__(self, text):
        self._text = text

    def split(self):
        pages = []
        page = ""
        nwords = 0
        nchars = 0
        for c in self._text:
            if self.ENDOFWORD.find(c) != -1:
                nwords = nwords + 1
            nchars = nchars + 1
            page = page + c
            if (nwords == self.HARD_MAX_WORDS or
                    nchars == self.HARD_MAX_CHARS or
                    (self.ENDOFPAGE.find(c) != -1 and (
                        nwords >= self.SOFT_MAX_WORDS or
                        nchars >= self.SOFT_MAX_CHARS))):
                pages.append(page)
                nchars = 0
                nwords = 0
                page = ""
        if page:
            pages.append(page)
        return pages

def parseargs():
    parser = argparse.ArgumentParser(description='Generate a video from a text file.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('input', type=str, nargs=1,
                        help='input text file')
    parser.add_argument('output', type=str, nargs=1,
                        help='output video file')

    parser.add_argument('--frame_rate', dest='frame_rate', action='store',
                        type=int, default=25, help="video fps")
    parser.add_argument('--lang', dest='lang', action='store',
                        type=str, default="es-ES", help="voice language")
    parser.add_argument('--tmpdir', dest='tmpdir', action='store',
                        type=str, default="tmp", help="doesn't need to exists, but its current contents will be removed!")
    parser.add_argument('--font', dest='font', action='store',
                        type=str, default="Helvetica", help="text font")

    args = parser.parse_args()
    return (args.input[0], args.output[0], args.frame_rate, args.lang, args.tmpdir, args.font)

(inputfile, outputfile, FRAME_RATE, LANG, TMPDIR, FONT) = parseargs()

SIZE = "640x360"


try:
    tmpfiles = glob.glob(os.path.join(TMPDIR, '*'))
    for f in tmpfiles:
        os.remove(f)
    os.rmdir(TMPDIR)
except Exception:
    pass

os.mkdir(TMPDIR)
print ("Reading %s"% (inputfile))
f = open(inputfile, 'r')
filecontents = f.read()
f.close()

split = Splitter2(filecontents)

vi = VideoImages(os.path.join(TMPDIR, "vi%05d.jpg"))
vs = VideoSpeech(os.path.join(TMPDIR, "vs.wav"))

for page in split.split():
    dur = vs.generate(page)
    vi.generate(page, int(dur*FRAME_RATE))

call(["ffmpeg", "-y",
      "-r", str(FRAME_RATE),
      "-i", os.path.join(TMPDIR, "vi%05d.jpg"),
      "-i", os.path.join(TMPDIR, "vs.wav"),
      "-strict", "-2",
      "-pix_fmt", "yuv420p",
      outputfile])

tmpfiles = glob.glob(os.path.join(TMPDIR, '*'))
for f in tmpfiles:
    os.remove(f)
os.rmdir(TMPDIR)
